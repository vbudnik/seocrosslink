<?php


namespace Hunters\SeoLink\Block;


use Magento\Framework\View\Element\Template;

class Catalog extends \Magento\Framework\View\Element\Template
{

    const CATEGORY_CROSS_LINK    =   'category_cross_link';

    private $_registry;
    private $_jsonHelper;

    public function __construct(
        \Magento\Framework\Registry         $registry,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Template\Context                    $context,
        array                               $data = []
    )
    {
        $this->_jsonHelper  =   $jsonHelper;
        $this->_registry    =   $registry;
        parent::__construct($context, $data);
    }

    public function getCurrentCategory() {
        return $this->_registry->registry('current_category');
    }

    public function getCategoryId() {
        return $this->getCurrentCategory()->getId();
    }

    public function getSeoCrossLinkAttribute() {
        return $this->getCurrentCategory()->getData(self::CATEGORY_CROSS_LINK);
    }

    public function getJsonSeoCrossLink() {
        $links = explode("\r\n", $this->getSeoCrossLinkAttribute());
        return $this->_jsonHelper->jsonEncode($links);
    }
}