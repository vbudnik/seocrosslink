<?php

namespace Hunters\SeoLink\Block;


class CustomContent extends \Magento\Cms\Block\Page
{

    protected function _toHtml()
    {
        $logger = \Magento\Framework\App\ObjectManager::getInstance()->create('Psr\Log\LoggerInterface');
        try {
            $links = [];
            $content = $this->_filterProvider->getPageFilter()->filter($this->getPage()->getSeoCrossLink());
            $links = explode("\r\n", $content);
            $string = <<<EOT
        <script id='remove' type="text/javascript">
        require(['jquery'], function ($) {
                var i = 0;
                var timer = setInterval(function () {
                    

EOT;
                foreach ($links as $link) {
                    $string .= "$('head').append('".$link."');";
                }
            $string .= <<<EOT
                        i++;
                        if (i) {
                                $("#remove").remove();
                                clearInterval(timer);
                        }
                }, 1000);
        });
    </script>
EOT;
                return $string;
        } catch (\Exception $e) {
            $logger->debug($e->getMessage());
        }
    }
}