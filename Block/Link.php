<?php


namespace Hunters\SeoLink\Block;


use Magento\Framework\View\Element\Template;

class Link extends \Magento\Framework\View\Element\Template
{

    const SEO_CROSS_LINK    =   'seo_cross_link';

    private $_registry;
    private $_jsonHelper;

    public function __construct(
        \Magento\Framework\Registry         $registry,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Template\Context                    $context,
        array                               $data = []
    )
    {
        $this->_jsonHelper  =   $jsonHelper;
        $this->_registry    =   $registry;
        parent::__construct($context, $data);
    }

    public function getCurrentProduct() {
        return $this->_registry->registry('current_product');
    }

    public function getProductId() {
        return $this->getCurrentProduct()->getId();
    }

    public function getSeoCrossLinkAttribute() {
        return $this->getCurrentProduct()->getData(self::SEO_CROSS_LINK);
    }

    public function getJsonSeoCrossLink() {
        $links = explode("\r\n", $this->getSeoCrossLinkAttribute());
        return $this->_jsonHelper->jsonEncode($links);
    }
}