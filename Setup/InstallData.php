<?php


namespace Hunters\SeoLink\Setup;


use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    private $_eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->_eavSetupFactory =   $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'seo_cross_link',
            [
                'group'                     =>  'Seo Cross Link',
                'input'			            =>	'textarea',
                'type'                      =>  'text',
                'backend'                   =>  '',
                'frontend'                  =>  '',
                'label'                     =>  'Seo Cross Link',
                'class'                     =>  '',
                'source'                    =>  '',
                'global'                    =>  \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_STORE,
                'visible'                   =>  true,
                'required'                  =>  false,
                'user_defined'              =>  true,
                'default'                   =>  '',
                'searchable'                =>  false,
                'filterable'                =>  false,
                'comparable'                =>  false,
                'visible_on_front'          =>  false,
                'used_in_product_listing'   =>  true,
                'unique'                    =>  false,
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'category_cross_link',
            [
                'type'      =>  'text',
                'input'     =>  'textarea',
                'label'     =>  'Category Cross Link',
                'source'    =>  '',
                'visible'   =>  true,
                'default'   =>  '',
                'required'  =>  false,
                'global'    =>  \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group'     =>  'Seo Cross Link'
            ]
        );
    }
}
