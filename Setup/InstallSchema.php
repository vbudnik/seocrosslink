<?php

namespace Hunters\SeoLink\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        $setup->getConnection()->addColumn(
            $setup->getTable('cms_page'),
            'seo_cross_link',
            [
                'type'      => \Magento\Framework\Db\Ddl\Table::TYPE_TEXT,
                'length'    => '2M',
                'nullable'  => true,
                'comment'   => 'SEO Cross Link'
            ]
        );

        $setup->endSetup();
    }
}

